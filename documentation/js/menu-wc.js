'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">cgm-1 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AdminModule.html" data-type="entity-link" >AdminModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"' : 'data-target="#xs-components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"' :
                                            'id="xs-components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"' }>
                                            <li class="link">
                                                <a href="components/AdminComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AdminComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"' : 'data-target="#xs-components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"' :
                                            'id="xs-components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogModule.html" data-type="entity-link" >CatalogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"' : 'data-target="#xs-components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"' :
                                            'id="xs-components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"' }>
                                            <li class="link">
                                                <a href="components/CatalogComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogFooterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogFooterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogListItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CatalogListItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ContactsModule.html" data-type="entity-link" >ContactsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"' : 'data-target="#xs-components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"' :
                                            'id="xs-components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"' }>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContactsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link" >CoreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"' : 'data-target="#xs-components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"' :
                                            'id="xs-components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"' }>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"' : 'data-target="#xs-components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"' :
                                            'id="xs-components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LostPassComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LostPassComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegistrationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegistrationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignInComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SignInComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/MapsDemoModule.html" data-type="entity-link" >MapsDemoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"' : 'data-target="#xs-components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"' :
                                            'id="xs-components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"' }>
                                            <li class="link">
                                                <a href="components/MapsDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MapsDemoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"' : 'data-target="#xs-components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"' :
                                            'id="xs-components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LeafletComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LeafletComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ModalComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ModalComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PanelComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PanelComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SuperTabBarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SuperTabBarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UikitModule.html" data-type="entity-link" >UikitModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"' : 'data-target="#xs-components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"' :
                                            'id="xs-components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"' }>
                                            <li class="link">
                                                <a href="components/AccordionDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionDemoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ButtonsDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ButtonsDemoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PanelsDemoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PanelsDemoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UikitComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UikitComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/CatalogService.html" data-type="entity-link" >CatalogService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Address.html" data-type="entity-link" >Address</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Company.html" data-type="entity-link" >Company</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Geo.html" data-type="entity-link" >Geo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link" >User</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});