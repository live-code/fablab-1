'use strict';

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct.bind(); } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

customElements.define('compodoc-menu', /*#__PURE__*/function (_HTMLElement) {
  _inherits(_class, _HTMLElement);

  var _super = _createSuper(_class);

  function _class() {
    var _this;

    _classCallCheck(this, _class);

    _this = _super.call(this);
    _this.isNormalMode = _this.getAttribute('mode') === 'normal';
    return _this;
  }

  _createClass(_class, [{
    key: "connectedCallback",
    value: function connectedCallback() {
      this.render(this.isNormalMode);
    }
  }, {
    key: "render",
    value: function render(isNormalMode) {
      var tp = lithtml.html("\n        <nav>\n            <ul class=\"list\">\n                <li class=\"title\">\n                    <a href=\"index.html\" data-type=\"index-link\">cgm-1 documentation</a>\n                </li>\n\n                <li class=\"divider\"></li>\n                ".concat(isNormalMode ? "<div id=\"book-search-input\" role=\"search\"><input type=\"text\" placeholder=\"Type to search\"></div>" : '', "\n                <li class=\"chapter\">\n                    <a data-type=\"chapter-link\" href=\"index.html\"><span class=\"icon ion-ios-home\"></span>Getting started</a>\n                    <ul class=\"links\">\n                        <li class=\"link\">\n                            <a href=\"overview.html\" data-type=\"chapter-link\">\n                                <span class=\"icon ion-ios-keypad\"></span>Overview\n                            </a>\n                        </li>\n                        <li class=\"link\">\n                            <a href=\"index.html\" data-type=\"chapter-link\">\n                                <span class=\"icon ion-ios-paper\"></span>README\n                            </a>\n                        </li>\n                                <li class=\"link\">\n                                    <a href=\"dependencies.html\" data-type=\"chapter-link\">\n                                        <span class=\"icon ion-ios-list\"></span>Dependencies\n                                    </a>\n                                </li>\n                                <li class=\"link\">\n                                    <a href=\"properties.html\" data-type=\"chapter-link\">\n                                        <span class=\"icon ion-ios-apps\"></span>Properties\n                                    </a>\n                                </li>\n                    </ul>\n                </li>\n                    <li class=\"chapter modules\">\n                        <a data-type=\"chapter-link\" href=\"modules.html\">\n                            <div class=\"menu-toggler linked\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#modules-links"' : 'data-target="#xs-modules-links"', ">\n                                <span class=\"icon ion-ios-archive\"></span>\n                                <span class=\"link-name\">Modules</span>\n                                <span class=\"icon ion-ios-arrow-down\"></span>\n                            </div>\n                        </a>\n                        <ul class=\"links collapse \" ").concat(isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"', ">\n                            <li class=\"link\">\n                                <a href=\"modules/AdminModule.html\" data-type=\"entity-link\" >AdminModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"' : 'data-target="#xs-components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"' : 'id="xs-components-links-module-AdminModule-9e6e841f414c456f2963e33e25a88a833c248917d9a65b6edba444f91b231db96d437fbbd9325aa398f4ccaa64916a6fd648e87f7cf543ef72033401db0d3ac1"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/AdminComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >AdminComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/AppModule.html\" data-type=\"entity-link\" >AppModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"' : 'data-target="#xs-components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"' : 'id="xs-components-links-module-AppModule-c81e9fa7521e6e05f1d2131cb7fb8e1f4cb07e212f980f9a1ffc15ffb2f22f87c6047c0d306579f392a9986a57d28f35b4cd2325ac62e49254f361f001390333"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/AppComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >AppComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/AppRoutingModule.html\" data-type=\"entity-link\" >AppRoutingModule</a>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/CatalogModule.html\" data-type=\"entity-link\" >CatalogModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"' : 'data-target="#xs-components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"' : 'id="xs-components-links-module-CatalogModule-c006228ab944d48a586f2af2adf73f876d879d249115d17ffb80303431af6aaf74e0ab77a5a0866281b287eeac18290045f9c3145dda28738add717b11beb5c6"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/CatalogComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >CatalogComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/CatalogFooterComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >CatalogFooterComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/CatalogFormComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >CatalogFormComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/CatalogListComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >CatalogListComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/CatalogListItemComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >CatalogListItemComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/ContactsModule.html\" data-type=\"entity-link\" >ContactsModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"' : 'data-target="#xs-components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"' : 'id="xs-components-links-module-ContactsModule-b10c776bd70466fd30ba9106216267a646b81d6bb7411fd6b8340fe35ebbf65ab3b7ccf03978a9ae8d9f334fe2981c4a2d4003b467686cf2b74dbcfef5d43a5f"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/ContactsComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >ContactsComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/CoreModule.html\" data-type=\"entity-link\" >CoreModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"' : 'data-target="#xs-components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"' : 'id="xs-components-links-module-CoreModule-382839d14b65eb9df8e606aa070996464afb80b893f9e4b861b90c43d0331f682f5a5023382658daa0263183e962d6c75878b2e48b69d4e1df523a38c399b279"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/NavbarComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >NavbarComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/LoginModule.html\" data-type=\"entity-link\" >LoginModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"' : 'data-target="#xs-components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"' : 'id="xs-components-links-module-LoginModule-3b67ea7fcb22b09bebd4899e2b8d061efc24ae4f7d818da60e5a055a814c2e2fad0117e053a4c8352a175cd4887bb77abfe847d7f11803009ebfa390056fd108"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/LoginComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >LoginComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/LostPassComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >LostPassComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/RegistrationComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >RegistrationComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/SignInComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >SignInComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/MapsDemoModule.html\" data-type=\"entity-link\" >MapsDemoModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"' : 'data-target="#xs-components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"' : 'id="xs-components-links-module-MapsDemoModule-585b1d7a916c7dd3b71595759c5d276affb8a954128cbd579841ecdb27a4af914eb804ff3585b1ecad80761d68743514b3aa86113d0f396679efe2899d8ea930"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/MapsDemoComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >MapsDemoComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/SharedModule.html\" data-type=\"entity-link\" >SharedModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"' : 'data-target="#xs-components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"' : 'id="xs-components-links-module-SharedModule-487f404101d188f0be395323d0f60a0466dce13158329902f5bdbcc558ffd1505d5389829cf66dc2c1d5d9568008d98288994aa7ce4bd1f61145cefeb08c9692"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/AccordionComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >AccordionComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/CardComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >CardComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/LeafletComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >LeafletComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/ModalComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >ModalComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/PanelComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >PanelComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/SuperTabBarComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >SuperTabBarComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"modules/UikitModule.html\" data-type=\"entity-link\" >UikitModule</a>\n                                    <li class=\"chapter inner\">\n                                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"' : 'data-target="#xs-components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"', ">\n                                            <span class=\"icon ion-md-cog\"></span>\n                                            <span>Components</span>\n                                            <span class=\"icon ion-ios-arrow-down\"></span>\n                                        </div>\n                                        <ul class=\"links collapse\" ").concat(isNormalMode ? 'id="components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"' : 'id="xs-components-links-module-UikitModule-0f7d2288dc6ab9e5f966b6d4694933fe2a872e71958497dd6f61065013eee1ed78de4a78b18d7d6fa7656de230ed76d5c4a059453edc02a57b5be4c216451469"', ">\n                                            <li class=\"link\">\n                                                <a href=\"components/AccordionDemoComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >AccordionDemoComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/ButtonsDemoComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >ButtonsDemoComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/PanelsDemoComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >PanelsDemoComponent</a>\n                                            </li>\n                                            <li class=\"link\">\n                                                <a href=\"components/UikitComponent.html\" data-type=\"entity-link\" data-context=\"sub-entity\" data-context-id=\"modules\" >UikitComponent</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                            </li>\n                </ul>\n                </li>\n                        <li class=\"chapter\">\n                            <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#injectables-links"' : 'data-target="#xs-injectables-links"', ">\n                                <span class=\"icon ion-md-arrow-round-down\"></span>\n                                <span>Injectables</span>\n                                <span class=\"icon ion-ios-arrow-down\"></span>\n                            </div>\n                            <ul class=\"links collapse \" ").concat(isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"', ">\n                                <li class=\"link\">\n                                    <a href=\"injectables/CatalogService.html\" data-type=\"entity-link\" >CatalogService</a>\n                                </li>\n                            </ul>\n                        </li>\n                    <li class=\"chapter\">\n                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#interfaces-links"' : 'data-target="#xs-interfaces-links"', ">\n                            <span class=\"icon ion-md-information-circle-outline\"></span>\n                            <span>Interfaces</span>\n                            <span class=\"icon ion-ios-arrow-down\"></span>\n                        </div>\n                        <ul class=\"links collapse \" ").concat(isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"', ">\n                            <li class=\"link\">\n                                <a href=\"interfaces/Address.html\" data-type=\"entity-link\" >Address</a>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"interfaces/Company.html\" data-type=\"entity-link\" >Company</a>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"interfaces/Geo.html\" data-type=\"entity-link\" >Geo</a>\n                            </li>\n                            <li class=\"link\">\n                                <a href=\"interfaces/User.html\" data-type=\"entity-link\" >User</a>\n                            </li>\n                        </ul>\n                    </li>\n                    <li class=\"chapter\">\n                        <div class=\"simple menu-toggler\" data-toggle=\"collapse\" ").concat(isNormalMode ? 'data-target="#miscellaneous-links"' : 'data-target="#xs-miscellaneous-links"', ">\n                            <span class=\"icon ion-ios-cube\"></span>\n                            <span>Miscellaneous</span>\n                            <span class=\"icon ion-ios-arrow-down\"></span>\n                        </div>\n                        <ul class=\"links collapse \" ").concat(isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"', ">\n                            <li class=\"link\">\n                                <a href=\"miscellaneous/variables.html\" data-type=\"entity-link\">Variables</a>\n                            </li>\n                        </ul>\n                    </li>\n                        <li class=\"chapter\">\n                            <a data-type=\"chapter-link\" href=\"routes.html\"><span class=\"icon ion-ios-git-branch\"></span>Routes</a>\n                        </li>\n                    <li class=\"chapter\">\n                        <a data-type=\"chapter-link\" href=\"coverage.html\"><span class=\"icon ion-ios-stats\"></span>Documentation coverage</a>\n                    </li>\n                    <li class=\"divider\"></li>\n                    <li class=\"copyright\">\n                        Documentation generated using <a href=\"https://compodoc.app/\" target=\"_blank\">\n                            <img data-src=\"images/compodoc-vectorise.png\" class=\"img-responsive\" data-type=\"compodoc-logo\">\n                        </a>\n                    </li>\n            </ul>\n        </nav>\n        "));
      this.innerHTML = tp.strings;
    }
  }]);

  return _class;
}( /*#__PURE__*/_wrapNativeSuper(HTMLElement)));