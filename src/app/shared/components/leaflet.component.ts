import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input, NgZone,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import * as L from "leaflet";

/**
 * Leaflet component... è bello
 */
@Component({
  selector: 'cgm-leaflet',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div #map class="map"></div>
    {{render()}}
  `,
  styles: [`
    .map { height: 180px;  }
  `]
})
export class LeafletComponent implements OnChanges {
  @ViewChild('map',{static: true}) mapRef!: ElementRef<HTMLDivElement>;
  /**
   * Le coordinate da mostrare nel component
   */
  @Input() coords!: [number, number];
  @Input() zoom: number = 5;
  map!: L.Map;

  constructor(private _ngZone: NgZone) {}


  initMap() {
    this._ngZone.runOutsideAngular(() => {
      this.map = L.map(this.mapRef.nativeElement)
        .setView(this.coords, this.zoom);

      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
      }).addTo(this.map);
    });




  }

  ngOnChanges(changes: SimpleChanges) {
    const { coords, zoom } = changes;

    if (!this.map) {
      this.initMap()
    }

    if (coords) {
      this.map.setView(coords.currentValue)
    }

    if (zoom) {
      this.map.setZoom(this.zoom)
    }
  }


  render() {
    console.log('render')
  }
}
