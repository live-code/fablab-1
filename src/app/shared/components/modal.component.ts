import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'cgm-modal',
  template: `
    <div class="fixed">
      <div class="border border-black bg-slate-200 rounded-xl p-3">
        <div
          class="flex justify-between items-center"
        >
          <div class="text-xl">{{title}}</div>
          <div
            *ngIf="closeIcon"
            (click)="close.emit()"
          >{{closeIcon}}</div>
        </div>

        <ng-content select=".body"></ng-content>

        <div class="bg-slate-400">
          <ng-content select=".footer"></ng-content>
        </div>
      </div>
    </div>
  `,
})
export class ModalComponent  {
  @Input() title = '';
  @Input() closeIcon: string | undefined;
  @Output() close = new EventEmitter()

}
