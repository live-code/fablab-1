import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChild,
  ContentChildren, Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {CardComponent} from "./card.component";

@Component({
  selector: 'cgm-accordion',
  template: `
    <ng-content></ng-content>
  `,
  styles: [
  ]
})
export class AccordionComponent implements AfterContentInit {
  @Input() openOnce: boolean = true
  @ContentChildren(CardComponent) cards!: QueryList<CardComponent>



  ngAfterContentInit() {
    this.cards.toArray().forEach(c => {
      c.headerClick.subscribe(() => {
        if(this.openOnce) {
          this.closeAll()
          c.isOpened = true
        } else {
          c.isOpened = !c.isOpened
        }
      })
      c.isOpened = false
    })
    this.cards.toArray()[0].isOpened = true
  }

  private closeAll() {
    this.cards.toArray().forEach(c => {
      c.isOpened = false
    })
  }

}
