import {Component, EventEmitter, Input, OnInit, Optional, Output} from '@angular/core';
import {AccordionComponent} from "./accordion.component";

@Component({
  selector: 'cgm-card',
  template: `
    <div class="border border-black bg-slate-200 rounded-xl p-3">
      <div
        class="flex justify-between items-center"
        (click)="headerClick.emit()"
      >
        <div class="text-xl">{{title}}</div>
        <div *ngIf="icon">{{icon}}</div>
      </div>
      <br>
      <div *ngIf="isOpened">
        <ng-content></ng-content>
      </div>
    </div>


  `,
})
export class CardComponent  {
  @Input() title = '';
  @Input() icon: string | undefined;
  @Input() isOpened = true;
  @Output() headerClick = new EventEmitter();


  constructor(@Optional() public accordion: AccordionComponent) {
  }

  ngOnInit() {

  }
}
