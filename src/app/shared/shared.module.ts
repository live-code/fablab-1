import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PanelComponent} from "./components/panel.component";
import { SuperTabBarComponent } from './components/super-tab-bar.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CardComponent } from './components/card.component';
import {ModalComponent} from "./components/modal.component";
import { AccordionComponent } from './components/accordion.component';
import { LeafletComponent } from './components/leaflet.component';

@NgModule({
  declarations: [
    PanelComponent,
    SuperTabBarComponent,
    CardComponent,
    ModalComponent,
    AccordionComponent,
    LeafletComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PanelComponent,
    SuperTabBarComponent,
    CardComponent,
    ModalComponent,
    AccordionComponent,
    LeafletComponent
  ]
})
export class SharedModule { }
