import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'catalog',
    loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)
  },
  {path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule)},
  {path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule)},
  {path: '', redirectTo: 'catalog', pathMatch: 'full'},
  { path: 'admin', loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
