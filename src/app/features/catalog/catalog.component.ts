import { Component, OnInit } from '@angular/core';
import {CatalogService} from "./services/catalog.service";

@Component({
  selector: 'cgm-catalog',
  template: `
    <h1>Catalog</h1>

    <cgm-catalog-form
      [activeUser]="catalogService.selectedUser"
      (save)="catalogService.save($event)"
      (reset)="catalogService.reset()"
    ></cgm-catalog-form>

    <cgm-catalog-list
      [activeUser]="catalogService.selectedUser"
      [items]="catalogService.users"
      (itemClick)="catalogService.selectUser($event)"
      (deleteUser)="catalogService.deleteUser($event)"
    ></cgm-catalog-list>
  `,
})
export class CatalogComponent {
  constructor(public catalogService: CatalogService) {
    catalogService.getAll()
  }

}
