import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../../model/user";
import {delay} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  title = 'pippo'
  users: Partial<User>[] = [];
  selectedUser: Partial<User> = {}

  constructor(private http: HttpClient) {}

  getAll() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  deleteUser(id: number) {
    this.http.delete(`http://localhost:3000/users/${id}`)
      .subscribe(() => {
        this.users = this.users.filter(p => p.id !== id)

        if (id === this.selectedUser?.id)
          this.reset()
      })
  }

  save(user: Partial<User>) {
    if (this.selectedUser?.id) {
      this.editUser(user)
    } else {
      this.addUser(user)
    }
  }

  addUser(user: Partial<User>) {
    this.http.post<Partial<User>>(`http://localhost:3000/users`, user)
      .subscribe(res => {
        this.users = [...this.users, res]
        this.reset()
      })
  }

  editUser(user: Partial<User>) {
    this.http.patch<any>(`http://localhost:3000/users/${this.selectedUser?.id}`, user)
      .pipe(delay(1000))
      .subscribe(res => {
        this.users = this.users.map(u => {
          return u.id === this.selectedUser?.id ? res : u;
        })
      })
  }

  selectUser(user: Partial<User>) {
    console.log('SELECT')
    this.selectedUser = user;
  }

  reset() {
    this.selectedUser = {};
  }
}


