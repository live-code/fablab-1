import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {User} from "../../../model/user";

@Component({
  selector: 'cgm-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <li
        *ngFor="let p of items"
        (click)="itemClick.emit(p)"
        [ngClass]="{active: p.id === activeUser?.id}"
    >
        {{p.name}}

      <button (click)="deleteHandler($event, p.id!)">Delete</button>
    </li>
  `,
  styles: [`
    .active { background-color: cyan}
  `]
})
export class CatalogListComponent {
  @Input() activeUser: Partial<User> | null = null
  @Input() items: Partial<User>[] | undefined;
  @Output() deleteUser = new EventEmitter<number>();
  @Output() itemClick = new EventEmitter<Partial<User>>();

  deleteHandler(e: MouseEvent, id: number) {
    e.stopPropagation()
    this.deleteUser.emit(id)
  }
}
