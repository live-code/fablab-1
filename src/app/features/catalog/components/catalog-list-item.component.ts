import { Component, OnInit } from '@angular/core';
import {CatalogService} from "../services/catalog.service";

@Component({
  selector: 'cgm-catalog-list-item',
  template: `
    <p>
      catalog-list-item works!
    </p>
    {{render()}}

  `,
  styles: [
  ]
})
export class CatalogListItemComponent implements OnInit {

  constructor(public catalogService: CatalogService) { }

  ngOnInit(): void {
  }

  render() {
   // console.log('LIst Item: (render')
  }
}
