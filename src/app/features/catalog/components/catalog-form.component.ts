import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {User} from "../../../model/user";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'cgm-catalog-form',
  template: `
    <form [formGroup]="form" (submit)="saveHandler()">
      <input type="text" formControlName="name">
      <input type="text" formControlName="email">
      <button
        type="submit" [disabled]="form.invalid"
        [style.background-color]="activeUser?.id ? 'red' : 'cyan'"
      >
        {{activeUser?.id ? 'EDIT' : 'ADD'}}
      </button>

      <button type="button" (click)="reset.emit()">Reset</button>

    </form>

    {{activeUser | json}}
  `,
})
export class CatalogFormComponent implements OnChanges {
  @Input() activeUser: Partial<User> = {}
  @Output() save = new EventEmitter<Partial<User>>();
  @Output() reset = new EventEmitter();

  form = this.fb.nonNullable.group({
    name: ['', [Validators.required, Validators.minLength(3)]],
    email: ''
  })

  constructor(private fb: FormBuilder) {}

  ngOnChanges(){
    if (this.activeUser?.id ) {
      this.form.patchValue(this.activeUser)
    } else {
      this.form.reset();
    }
  }

  saveHandler() {
    this.save.emit(this.form.value)
    // this.form.reset()
  }
}
