import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogFooterComponent } from './components/catalog-footer.component';
import {CatalogComponent} from "./catalog.component";
import {CatalogListComponent} from "./components/catalog-list.component";
import {CatalogFormComponent} from "./components/catalog-form.component";
import {CatalogListItemComponent} from "./components/catalog-list-item.component";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../shared/shared.module";



@NgModule({
  declarations: [
    CatalogComponent,
      CatalogListComponent,
        CatalogListItemComponent,
      CatalogFormComponent,
      CatalogFooterComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: CatalogComponent }
    ])
  ],

})
export class CatalogModule { }

