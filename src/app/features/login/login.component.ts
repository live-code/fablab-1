import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgm-login',
  template: `
    <h1>Login</h1>
    <router-outlet></router-outlet>

    <hr>
    <button routerLink="sign-in">SignIn</button>
    <button routerLink="lost-pass">LostPass</button>
    <button routerLink="registration">Registration</button>

  `,
})
export class LoginComponent {


}
