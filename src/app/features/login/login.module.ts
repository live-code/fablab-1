import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import {RouterModule} from "@angular/router";
import { SignInComponent } from './components/sign-in.component';
import { LostPassComponent } from './components/lost-pass.component';
import { RegistrationComponent } from './components/registration.component';



@NgModule({
  declarations: [
    LoginComponent,
    SignInComponent,
    LostPassComponent,
    RegistrationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: LoginComponent,
        children: [
          { path: 'lost-pass', component: LostPassComponent},
          { path: 'sign-in', component: SignInComponent},
          { path: 'registration', component: RegistrationComponent},
        ]
      }
    ])
  ]
})
export class LoginModule { }
