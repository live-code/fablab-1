import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapsDemoComponent } from './maps-demo.component';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../../../shared/shared.module";



@NgModule({
  declarations: [
    MapsDemoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: '', component: MapsDemoComponent}
    ]),
    SharedModule
  ]
})
export class MapsDemoModule { }
