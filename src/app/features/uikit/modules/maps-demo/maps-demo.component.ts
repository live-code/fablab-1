import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'cgm-maps-demo',
  template: `
    <h1>maps-demo works!</h1>

    <cgm-leaflet
      [zoom]="value"
      [coords]="myCoords"></cgm-leaflet>
    <button (click)="myCoords = [44, 12]">Location 1 </button>
    <button (click)="myCoords = [44, 12.5]">Location 2 </button>
    <button (click)="value = value + 1">+</button>
    <button (click)="value = value - 1">-</button>
  `,

})
export class MapsDemoComponent  {
  myCoords: [number, number] = [43, 13];
  value = 10;

}
