import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule, Route} from '@angular/router';
import { UikitComponent } from './uikit.component';
import { AccordionDemoComponent } from './components/accordion-demo.component';
import { PanelsDemoComponent } from './components/panels-demo.component';
import { ButtonsDemoComponent } from './components/buttons-demo.component';
import {SharedModule} from "../../shared/shared.module";

const routes: Routes = [
  {
    path: '', component: UikitComponent,
    children: [
      { path: 'buttons', component: ButtonsDemoComponent},
      { path: 'panels', component: PanelsDemoComponent},
      { path: 'accordions', component: AccordionDemoComponent},
      { path: 'maps', loadChildren: () => import('./modules/maps-demo/maps-demo.module').then(m => m.MapsDemoModule)},
    ]
  }
]
@NgModule({
  declarations: [
    UikitComponent,
    AccordionDemoComponent,
    PanelsDemoComponent,
    ButtonsDemoComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class UikitModule { }
