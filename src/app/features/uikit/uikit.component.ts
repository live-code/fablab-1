import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgm-uikit',
  template: `

    <div class="flex gap-2">
      <button
        class="btn"
        routerLink="accordions">accordion</button>
      <button
        class="btn"
        routerLink="panels">panels</button>
      <button
        class="btn"
        routerLink="buttons">buttons</button>
      <button
        class="btn"
        routerLink="maps">maps</button>
    </div>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class UikitComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
