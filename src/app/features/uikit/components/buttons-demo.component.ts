import {Component, ComponentFactoryResolver, OnInit, Type, ViewContainerRef} from '@angular/core';
import {CardComponent} from "../../../shared/components/card.component";
import {LeafletComponent} from "../../../shared/components/leaflet.component";

@Component({
  selector: 'cgm-buttons-demo',
  template: `
    <h1>Dynamic Loaders</h1>



    <cgm-card title="ciao"></cgm-card>

    <b></b>
    bla bla


    .....
  `,
})
export class ButtonsDemoComponent  {

  constructor(private vcr: ViewContainerRef) { }

  json: any = [
    {
      type: 'leaflet',
      data: {
        coords: [43, 13],
        zoom: 10
      }
    },
    {
      type: 'card',
      data: {
        title: 'hello card'
      }
    },
    {
      type: 'leaflet',
      data: {
        coords: [43, 13],
        zoom: 4
      }
    },
  ]

  ngOnInit(): void {

    for (let config of this.json) {
      const compo = this.vcr.createComponent(COMPONENTS[config.type]);
      for (let key in config.data) {
        compo.setInput(key, config.data[key])
      }
    }

  }

}

export const COMPONENTS: { [key: string]: Type<any> } = {
  'card': CardComponent,
  'leaflet': LeafletComponent
}
