import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {CardComponent} from "../../../shared/components/card.component";

@Component({
  selector: 'cgm-accordion-demo',
  template: `

    <cgm-accordion [openOnce]="false">
      <cgm-card title="Group 1" icon="♡" >
        abc
      </cgm-card>
      <cgm-card title="Group 2" icon="♡">
        abc
      </cgm-card>
      <cgm-card title="Group 3" icon="♡">
        abc
      </cgm-card>
    </cgm-accordion>
  `,
})
export class AccordionDemoComponent  {

}
