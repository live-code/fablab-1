import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cgm-panels-demo',
  template: `
    <br>
    <div class="max-w-screen-xl sm:max-w-screen-md mx-auto">

      <button class="btn" (click)="show = !show">
        Show / Hide modal
      </button>

      <cgm-modal
        title="My DIalog"
        closeIcon="💩"
        *ngIf="show"
        (close)="show = false"
      >
        <div class="body">
          <input type="text">
          <input type="text">
          <input type="text">
        </div>

        <div class="footer">
          copyright FB 2022
        </div>


      </cgm-modal>

      <br>

      Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem beatae, blanditiis consequatur, dolores eos illum in incidunt ipsa natus obcaecati praesentium quas quasi ratione repellat temporibus totam voluptatum! Perspiciatis?
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem beatae, blanditiis consequatur, dolores eos illum in incidunt ipsa natus obcaecati praesentium quas quasi ratione repellat temporibus totam voluptatum! Perspiciatis?
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem beatae, blanditiis consequatur, dolores eos illum in incidunt ipsa natus obcaecati praesentium quas quasi ratione repellat temporibus totam voluptatum! Perspiciatis?
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. At autem beatae, blanditiis consequatur, dolores eos illum in incidunt ipsa natus obcaecati praesentium quas quasi ratione repellat temporibus totam voluptatum! Perspiciatis?

    </div>


  `,
  styles: [
  ]
})
export class PanelsDemoComponent implements OnInit {

  show = false;
  constructor() {
  }

  ngOnInit(): void {
  }

}
