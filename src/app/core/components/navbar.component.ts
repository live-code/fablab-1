import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {CatalogService} from "../../features/catalog/services/catalog.service";
import {User} from "../../model/user";

@Component({
  selector: 'cgm-navbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <button routerLink="catalog">Catalog</button>
    <button routerLink="login">Login</button>
    <button routerLink="uikit">uikit</button>
    {{render()}}
  `,
})
export class NavbarComponent {


  render() {
    console.log('Navbar: (render')
  }
}
