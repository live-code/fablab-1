import { Component } from '@angular/core';
import {CatalogService} from "./features/catalog/services/catalog.service";

@Component({
  selector: 'cgm-root',
  template: `
   <cgm-navbar [title]="catalogService.title"></cgm-navbar>
   <hr>

   <div>
        <router-outlet></router-outlet>
   </div>

  `,
  styles: [`
    .bg { background-color: red}
  `],
})
export class AppComponent {

  constructor(public catalogService: CatalogService) { }

}
